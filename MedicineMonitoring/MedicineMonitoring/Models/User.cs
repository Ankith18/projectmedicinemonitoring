﻿using MedicineMonitoring.ModelValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{

    public enum SecretQuestion
    {
        [Display(Name ="What elementary school did you attend ?")]
        School,
        [Display(Name = "What is the name of your first pet ?")]
        FirstPet,
        [Display(Name = "What is your favorite color?")]
        Color

    }
    public enum UserCategory
    {
        [Display(Name ="Admin")]
        Admin=1,
        [Display(Name = "Branch Admin")]
        BranchAdmin =2,
        [Display(Name = "Customer")]
        Customer =3

    }
    public enum Gender
    {
        [Display(Name = "Male")]
        Male = 1,
        [Display(Name = "Female")]
        Female = 2,
        [Display(Name = "Others")]
        Others = 3
    }
    public class User
    {


        [Required(ErrorMessage = " First Name is Required")]
        [Display(Name = "First Name")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets Allowed")]
        public string FirstName { get; set; }



        [Required(ErrorMessage = " Last Name is Required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets Allowed")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DobValidation]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
                        ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        [Display(Name = "Gender")]
        public Gender Gender { set; get; }

        [Key]

        [MinLength(8, ErrorMessage = "Minium length 8 chracter")]

        [Required(ErrorMessage = "UserId is required.")]
        [RegularExpression("^[a-zA-Z0-9@]*$", ErrorMessage = "Only Alphabets,Numbers and @ Allowed.")]
        [DisplayName("User Name")]

        public string UserId { get; set; }



        [Required(ErrorMessage = "Please Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Please Select Secret Question")]
        [Display(Name = "Secret Question")]
        public SecretQuestion SecretQuestions { get; set; }

        [Required(ErrorMessage = "Please Enter Answer")]
        [Display(Name = "Answer")]
        public string Answer { get; set; }

        [Required(ErrorMessage = "Email Id Required")]
        [DisplayName("Email ID")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$",
        ErrorMessage = "Email Format is wrong")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Enter Contact No")]
        [Display(Name = "Mobile")]
        [StringLength(10, ErrorMessage = "The Mobile must contains 10 characters", MinimumLength = 10)]
        [DataType(DataType.PhoneNumber)]
        public string ContactNo { get; set; }

        [Required]
        [Display(Name = "User Category")]
        public UserCategory Usercategory { get; set; }


        [Display(Name = "Branch Id")]
        public string BranchId { get; set; }
        public Branch Branch { get; set; }




        public bool Approve { get; set; }
       
        public Nullable<DateTime> CreatedDate { get; set; }
      
        public Nullable<DateTime> ModefiedDate { get; set; }

        public bool IsDeleted { get; set; }

    }
}