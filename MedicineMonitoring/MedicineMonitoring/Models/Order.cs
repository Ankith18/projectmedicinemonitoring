﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public enum PaymentModes
    {
        Cash,
        Checks,
        DebitCards,
        CreditCards,
        MobilePayments,

    }
    public class Order
    {
        [Key]
        public string OrderItemId { get; set; }
       
        public string UserId { get; set; }
        public User User { get; set; }
        [Required(ErrorMessage = "DeliveryAddress Required")]
        [DisplayName("Delivery Address")]
        public string DeliveryAddress { get; set; }
        [Required(ErrorMessage = "OrderDate Required")]
        [DataType(DataType.Date)]
        public System.DateTime OrderDate { get; set; }
        [Required(ErrorMessage = "PaymentMode Required")]
        [DisplayName("Payment Modes")]
        public PaymentModes PaymentMode { get; set; }

        public List<OrderItem> OrderItems { get; set; }
    }
}