namespace MedicineMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMedicineRequestTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MedicineRequests",
                c => new
                    {
                        RequestId = c.Int(nullable: false, identity: true),
                        BranchId = c.String(maxLength: 128),
                        MedName = c.String(nullable: false),
                        Manufacturer = c.String(nullable: false),
                        Disease = c.String(nullable: false),
                        quantity = c.Int(nullable: false),
                        IsCreated = c.DateTime(),
                        IsEditied = c.DateTime(),
                        Approve = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.RequestId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .Index(t => t.BranchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MedicineRequests", "BranchId", "dbo.Branches");
            DropIndex("dbo.MedicineRequests", new[] { "BranchId" });
            DropTable("dbo.MedicineRequests");
        }
    }
}
