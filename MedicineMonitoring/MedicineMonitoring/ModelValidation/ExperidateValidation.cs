﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.ModelValidation
{
    //Expiry Date validation
    public class ExperidateValidation:ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Medicine medicine = new Medicine();
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                if (d != null)
                {
                    if (d > medicine.ManufactureDate)
                    {
                        return ValidationResult.Success;
                    }
                }
            }
            return new ValidationResult(ErrorMessage ?? "Please provide proper Expiry Date");
        }
    }
    //Manufacturing Date validation
    public class ManufucturingDate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Medicine medicine = new Medicine();
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                if (d != null)
                {
                    if (d <DateTime.UtcNow)
                    {
                        return ValidationResult.Success;
                    }
                }
            }
            return new ValidationResult(ErrorMessage ?? "Please provide proper Expiry Date");
        }
    }
}