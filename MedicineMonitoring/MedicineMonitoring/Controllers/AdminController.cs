﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MedicineMonitoring.Controllers
{
  
    public class AdminController : Controller
    {
        // GET: Admin
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
        //Home Page
        [AllowAnonymous]
        public ActionResult Home()
        {
            return View();
        }
        //Request from Branch Admin Viewed by Admin
        [Authorize(Roles = "Admin")]
        public ActionResult Request()
        {
            var list = methods.GetRequest();
            if (list.Count == 0)
            {
                ViewBag.msg = 1;
                return View();
            }
            else
            {

                return View(list);
            }
        }
        //Request Details for Branch admin where admin can approve 
        [Authorize(Roles="Admin")]
        public ActionResult RequestDetails(string id)
        {
            MedicineDBContext _dbcontext = new MedicineDBContext();
         
            Branch branch = _dbcontext.Branches.Find(id);
            User user = _dbcontext.Users.FirstOrDefault(X => X.BranchId == id);
            user.BranchId = branch.BranchId;
            user.Branch.BranchName = branch.BranchName;
            user.Branch.BranchAddress = branch.BranchAddress;
            return View(user);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult RequestDetails(User user)
        {
         
            MedicineDBContext _dbcontext = new MedicineDBContext();
            if (user != null)
            {
                User users = _dbcontext.Users.FirstOrDefault(x => x.UserId == user.UserId);
                users.Approve = true;
               
                methods.Save(_dbcontext);
                return RedirectToAction("Request");
            }
            return View();
        }
    }
}