﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MedicineMonitoring.Controllers
{
    [Authorize(Roles = "Customer")]
    public class CustomerController : Controller
    {
        
        MedicineDBContext _dbcontext = new MedicineDBContext();
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
        // GET: Customer
        //Customer search for Medicine
        public ActionResult SearchMedicine()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SearchMedicine(string MedicineName)
        {
            
            var _medList = _dbcontext.Medicines.Where(y => y.MedName.Contains(MedicineName) || y.Disease.Contains(MedicineName)).OrderBy(y => y.MedName).ThenBy(m => m.Manufacturer).ThenBy(n => n.BranchAvaliable).ThenBy(p => p.Address).ThenBy(c => c.cost);
            return View(_medList);
        }
        //Adding medicine to Cart by Customer
        public ActionResult AddToCart(int id)
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddToCart(int id,Cart cart)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
               
                Medicine medicine = _dbcontext.Medicines.FirstOrDefault(y => y.MedId == id);
                Cart cartObj = new Cart();
                cartObj.MedId = medicine.MedId;
                cartObj.UserId = authTicket.Name;
                cartObj.MedName = medicine.MedName;
                cartObj.Quantity = cart.Quantity;
                cartObj.Cost = medicine.cost;
                cartObj.Subtotal = (cart.Quantity) * medicine.cost;
                _dbcontext.Carts.Add(cartObj);
                methods.Save(_dbcontext);
                return RedirectToAction("CartList");
            }
           
        }
        //Cart list where all medicines are added by Customer
        public ActionResult CartList()
        {
            return View(_dbcontext.Carts.ToList());
        }
        //Customer can Delete cart
        public ActionResult DeleteCart(int id)
        {
            Cart cart = _dbcontext.Carts.Find(id);
            _dbcontext.Carts.Remove(cart);
            methods.Save(_dbcontext);
            return RedirectToAction("CartList");
        }
        //Customer entering details such as payment and Delivery Address
        public ActionResult AddToOrder(string id)
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddToOrder(string id,Order order)
        {
            Order orderObj = new Order();
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
               
                var cartlist = _dbcontext.Carts.Where(x => x.UserId == id).ToList();
                foreach(var x in cartlist)
                {
                    Medicine medicine = _dbcontext.Medicines.FirstOrDefault(y => y.MedId == x.MedId);
                    if (x.Quantity > medicine.quantity)
                    {
                        ModelState.AddModelError("", medicine.MedName + " not in stock");
                        return View();

                    }
                   
                }

                orderObj.UserId = id;
                orderObj.OrderDate = (System.DateTime.UtcNow);
                orderObj.DeliveryAddress = order.DeliveryAddress;
                orderObj.PaymentMode = order.PaymentMode;
                orderObj.OrderItemId= "ORDER" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year +DateTime.Now.Minute+DateTime.Now.Millisecond+DateTime.Now.Second;
                _dbcontext.Orders.Add(orderObj);
                methods.Save(_dbcontext);
                methods.AddItemToList(id, orderObj.OrderItemId);
               

                return RedirectToAction("OrderDisplay",new { id = orderObj.UserId,orderId= orderObj.OrderItemId });
            }
           
        }
        //Order details viwed by Customer
        public ActionResult OrderDisplay(string id,string orderId)
        {
           
           
           ViewBag.Items = _dbcontext.OrderItems.Where(x => x.OrderItemId == orderId);
            ViewBag.GrandTotal = _dbcontext.Carts.Where(x => x.UserId == id).Select(x => x.Subtotal).Sum();
            ViewBag.OrderId = orderId;
            Order order= _dbcontext.Orders.FirstOrDefault(x => x.OrderItemId == orderId);
            ViewBag.OrderDate = order.OrderDate;
            ViewBag.PaymentMode = order.PaymentMode;
            ViewBag.DeliveryAddress = order.DeliveryAddress;
            var OrderIte = _dbcontext.Carts.Where(x => x.UserId == id).ToList();
            
            foreach(var x in OrderIte)
            {
                Medicine medicine = _dbcontext.Medicines.FirstOrDefault(y => y.MedId == x.MedId);
                medicine.quantity = medicine.quantity - x.Quantity;
                _dbcontext.Carts.Remove(x);
                methods.Save(_dbcontext);
            }
            return View();
        }

    }
}