﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MedicineMonitoring.Controllers
{
    public class MedicineRequestController : Controller
    {
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
        MedicineDBContext _dbcontext = new MedicineDBContext();
        // GET: MedicineRequest

        //Status of request Viewed by Branch Admin
        [Authorize(Roles = "BranchAdmin")]
        public ActionResult BranchMedicineRequestList()
        {
            HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
            var list = _dbcontext.MedicineRequests.Where(x => x.BranchId == user.BranchId).ToList();
            return View(list);
        }
        //Raising request of Medcine by Branch Admin
        [Authorize(Roles = "BranchAdmin")]
        public ActionResult RaiseMedicineRequestList()
        {
           
            MedicineRequest medicineRequest = new MedicineRequest();
            return View(medicineRequest);
        }
        [Authorize(Roles = "BranchAdmin")]
        [HttpPost]
        public ActionResult RaiseMedicineRequestList(MedicineRequest medicineRequest)
        {
            HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
            if (ModelState.IsValid)
            {
                medicineRequest.Approve = false;
                medicineRequest.IsCreated = DateTime.UtcNow;
                medicineRequest.BranchId = user.BranchId;
                _dbcontext.MedicineRequests.Add(medicineRequest);
                methods.Save(_dbcontext);
                ViewBag.Message = "Your Request are submitted successfully";
                return View();
            }
            return View();
            
        }
        //Request list for Admin
        [Authorize(Roles = "Admin")]
        public ActionResult MedicineRequest()
        {
            var list = _dbcontext.MedicineRequests.Where(x=>x.Approve==false).ToList();
            if (list.Count == 0)
            {
                ViewBag.msg = 1;
                return View();
            }
            else
            {

                return View(list);
            }
           
        }
        //Approval of request of Medcine
        [Authorize(Roles = "Admin")]
        public ActionResult MedicineRequestApproval(int id)
        {
            MedicineRequest medicineRequest = _dbcontext.MedicineRequests.FirstOrDefault(t => t.RequestId == id);
            return View(medicineRequest);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
         public ActionResult MedicineRequestApproval(MedicineRequest medReq)
        {
            if (medReq != null)
            {
                MedicineRequest medicineRequest = _dbcontext.MedicineRequests.FirstOrDefault(x => x.RequestId == medReq.RequestId);
                medicineRequest.Approve = true;
                medicineRequest.IsEditied = DateTime.UtcNow;
                methods.Save(_dbcontext);
                return RedirectToAction("MedicineRequest");
            }
            return View();
        }

    }
}