﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.ViewModels
{
    public class PasswordReset
    {
        [MinLength(8, ErrorMessage = "Minium length 8 chracter")]

        [Required(ErrorMessage = "UserId is required.")]
        [RegularExpression("^[a-zA-Z0-9@]*$", ErrorMessage = "Only Alphabets,Numbers and @ Allowed.")]
        [DisplayName("User Name")]

       

        public string UserId { get; set; }


        [Required(ErrorMessage = "Please Select Secret Question")]
        [Display(Name = "Secret Question")]
        public SecretQuestion SecretQuestions { get; set; }

        [Required(ErrorMessage = "Please Enter Answer")]
        [Display(Name = "Answer")]
        public string Answer { get; set; }
    }
}