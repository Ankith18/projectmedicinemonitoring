﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicineMonitoring.Controllers
{
    public class HomeController : Controller
    {
        //Selecting roles based on thire Role
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Index(User user)
        {
            int roleId = (int)user.Usercategory;
            if (roleId==0)
            {
                ViewBag.msg = "Please select Role";
                return View();
            }
            else
            {
               

                if (roleId == 1)
                {
                    return RedirectToAction("Login", "Account");
                }
                else if (roleId == 2)
                {
                    return RedirectToAction("BranchAdminRegistration", "UserBranch");
                }
                else if (roleId == 3)
                {
                    return RedirectToAction("UserRegistration", "Users");
                }
                else
                {
                    ModelState.AddModelError(" ", "Please Select Role");
                    return View();
                }
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}