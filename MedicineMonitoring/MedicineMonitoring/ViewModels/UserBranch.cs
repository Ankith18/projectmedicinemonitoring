﻿using MedicineMonitoring.Models;
using MedicineMonitoring.ModelValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.ViewModels
{
    public class UserBranch
    {

        [Required(ErrorMessage = " First Name is Required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets Allowed.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }



        [Required(ErrorMessage = " Last Name is Required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets Allowed.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DobValidation]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
                        ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        [Display(Name = "Gender")]
        public Gender Gender { set; get; }

        [Key]

        [MinLength(8, ErrorMessage = "Minium length 8 chracter")]

        [Required(ErrorMessage = "UserId is required.")]
        [RegularExpression("^[a-zA-Z0-9@]*$", ErrorMessage = "Only Alphabets,Numbers and @ Allowed.")]
        [DisplayName("User Id")]

        public string UserId { get; set; }



        [Required(ErrorMessage = "Please Enter Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please Enter Confirm Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Please Select Secret Question")]
        [Display(Name = "Secret Question")]
        public SecretQuestion SecretQuestions { get; set; }

        [Required(ErrorMessage = "Please Enter Answer")]
        [Display(Name = "Answer")]
        public string Answer { get; set; }

        [Required(ErrorMessage = "Email Id Required")]
        [DisplayName("Email ID")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$",
        ErrorMessage = "Email Format is wrong")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Enter Contact No")]
        [Display(Name = "Mobile")]
        [StringLength(10, ErrorMessage = "The Mobile must contains 10 characters", MinimumLength = 10)]
        [DataType(DataType.PhoneNumber)]
        public string ContactNo { get; set; }

        [Required]
        [Display(Name = "User Category")]
        public UserCategory Usercategory { get; set; }

        public bool Approve { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }

        public Nullable<DateTime> ModefiedDate { get; set; }

        public bool IsDeleted { get; set; }
        public string BranchId { get; set; }

        [Required(ErrorMessage = "Branch Name is required")]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }

        [Required(ErrorMessage = "Enter Branch Address")]
        [StringLength(100)]
        [Display(Name = "Branch Address")]
        public string BranchAddress { get; set; }


        [Display(Name = "Request Id")]
        public string RequestId { get; set; }
    }
}