﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public class OrderItem
    {
        [Key]
        public string OrderItemsId { get; set; }
       
        public string OrderItemId { get; set; }
     
        [Required(ErrorMessage = "ProductId Required")]
        public int MedId { get; set; }
       
     
        public string MedicineName { get; set; }
       
        public int Price { get; set; }
        [Required(ErrorMessage = "Quantity Required")]
        public int Quantity { get; set; }
        [Required(ErrorMessage = "Total Required")]
        public int Total { get; set; }

       
    }
}