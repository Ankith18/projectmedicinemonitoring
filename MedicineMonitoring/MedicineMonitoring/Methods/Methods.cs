﻿using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicineMonitoring.Methods
{
    //Common methods where highly used
    [Authorize(Roles = "Admin")]
    public class Methods
    {
        MedicineDBContext _dbcontext = new MedicineDBContext();
        public void Save(MedicineDBContext userContext)
        {
            
            userContext.SaveChanges();
        }
        
        public List<Branch> GetRequest()
        {
            
            var user = _dbcontext.Users.Where(y => y.BranchId != null && y.Approve == false).ToList();
            List<Branch> branches = new List<Branch>();
            var list = branches;
            foreach (var x in user)
            {
                Branch branch = _dbcontext.Branches.FirstOrDefault(y => y.BranchId == x.BranchId);
                branches.Add(branch);
            }
            return branches;
        }

        public void AddItemToList(string id, string orderid)
        {

            var OrderIte = _dbcontext.Carts.Where(x => x.UserId == id).ToList();
           
            foreach (var x in OrderIte)
            {

                OrderItem orderItem = new OrderItem();
                orderItem.OrderItemsId = "ITEM" + x.CartId;
                orderItem.OrderItemId = orderid;
                orderItem.MedId = x.MedId;
                orderItem.MedicineName = x.MedName;
                orderItem.Price = (int)x.Cost;
                orderItem.Quantity = x.Quantity;
                orderItem.Total = (x.Quantity * orderItem.Price);
                _dbcontext.OrderItems.Add(orderItem);


            }
            Save(_dbcontext);


        }
    }
}