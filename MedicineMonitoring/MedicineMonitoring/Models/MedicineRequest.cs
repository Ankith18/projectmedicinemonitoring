﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public class MedicineRequest
    {
        [Key]
        public int RequestId { get; set; }
        public string BranchId { get; set; }
        public Branch Branch { get; set; }

        [Required(ErrorMessage = " Medicine Name is Required")]
        [Display(Name = "Medicine Name")]
        [RegularExpression("^[a-zA-Z|0-9 ]*$", ErrorMessage = "Only Alphabets and Numbers are allowed.")]
        public string MedName { get; set; }

        [Required(ErrorMessage = " Manufacturer Name is Required")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed.")]
        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }

        [Required(ErrorMessage = "Disease/illness Details are Required")]
        [Display(Name = "Disease/illness")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed.")]
        public string Disease { get; set; }

        [Required(ErrorMessage = "Quantity is required.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers are allowed.")]
        [Display(Name = "Quantity")]
        public int quantity { get; set; }

        public Nullable<DateTime> IsCreated { get; set; }
        public Nullable<DateTime> IsEditied { get; set; }

        public bool Approve { get; set; }
    }
}