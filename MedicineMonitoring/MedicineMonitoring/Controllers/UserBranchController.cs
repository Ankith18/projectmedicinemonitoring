﻿using MedicineMonitoring.Models;
using MedicineMonitoring.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedicineMonitoring.Controllers
{
    public class UserBranchController : Controller
    {
        // GET: UserBranch
        //Branch Admin Registration
        public ActionResult BranchAdminRegistration()
        {
            UserBranch userBranch = new UserBranch();
            return View(userBranch);
        }
        [HttpPost]
        public ActionResult BranchAdminRegistration(UserBranch userBranch)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                using (MedicineDBContext _dbcontext = new MedicineDBContext())
                {
                    User user1 = _dbcontext.Users.Find(userBranch.UserId);
                    Branch branch1 = _dbcontext.Branches.Find(userBranch.BranchId);
                    if (user1 == null)
                    {
                        if (branch1 == null)
                        {
                            User user = new User();
                            Branch branch = new Branch();
                            user.FirstName = userBranch.FirstName;
                            user.LastName = userBranch.LastName;
                            user.DOB = userBranch.DOB;
                            user.Gender = userBranch.Gender;
                            user.UserId = userBranch.UserId;
                            user.Password = userBranch.Password;
                            user.ConfirmPassword = userBranch.ConfirmPassword;
                            user.SecretQuestions = userBranch.SecretQuestions;
                            user.Usercategory = (UserCategory)2;
                            user.Answer = userBranch.Answer;
                            user.Email = userBranch.Email;
                            user.ContactNo = userBranch.ContactNo;
                            user.CreatedDate = DateTime.UtcNow;
                           
                            user.BranchId = userBranch.BranchId;
                           
                            branch.BranchId = userBranch.BranchId;
                            branch.BranchName = userBranch.BranchName;
                            branch.BranchAddress = userBranch.BranchAddress;
                            branch.RequestId = "REQ" + userBranch.BranchId;
                            _dbcontext.Branches.Add(branch);
                            _dbcontext.Users.Add(user);
                            _dbcontext.SaveChanges();
                            return RedirectToAction("Login", "Account");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Branch Id already exists.");
                            return View();
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "User Id already exists.");
                        return View();
                    }
                }
            }
        }
    }
}