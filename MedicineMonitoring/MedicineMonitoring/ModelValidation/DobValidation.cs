﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.ModelValidation
{
    //DOB Validation
    public class DobValidation : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime d = Convert.ToDateTime(value);
                if (d != null)
                {
                    int Age = DateTime.UtcNow.Year - d.Year;
                    if (Age>=18)
                    {
                        return ValidationResult.Success;
                    }
                }
            }
            return new ValidationResult(ErrorMessage ?? "Please provide proper DOB(Age must be greater than 17)");
        }

    }
   
    }

