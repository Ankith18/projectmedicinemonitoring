﻿using MedicineMonitoring.ModelValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MedicineMonitoring.Models
{
    public class Medicine
    {
        [Key]
        public int MedId { get; set; }

        [Required(ErrorMessage = " Medicine Name is Required")]
        [Display(Name = "Medicine Name")]
        [RegularExpression("^[a-zA-Z|0-9 ]*$", ErrorMessage = "Only Alphabets and Numbers are allowed.")]
        public string MedName { get; set; }



        [Required(ErrorMessage = " Manufacturer Name is Required")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed.")]
        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }

        [Required(ErrorMessage = "Disease/illness Details are Required")]
        [Display(Name = "Disease/illness")]
        [RegularExpression("^[a-zA-Z, ]*$", ErrorMessage = "Only Alphabets are allowed.")]
        public string Disease { get; set; }

        [Required(ErrorMessage = "Please Provide Branch Name")]
        [Display(Name = "Branches")]
        public string BranchAvaliable { get; set; }

        [Required(ErrorMessage = "Please Provide Address")]
        [Display(Name = "City")]
        public string Address { get; set; }

        [ManufucturingDate]
        [Required(ErrorMessage = " Manufacture Date is Required")]
        [Display(Name = "Manufacturing Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ManufactureDate { get; set; }

        [ExperidateValidation]
        [Required(ErrorMessage = " Expiry Date is Required")]
        [Display(Name = "Expiry Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpiryDate { get; set; }



        [Required(ErrorMessage = "Cost is required.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers are allowed.")]
        [Display(Name = "Cost")]

        public float cost { get; set; }


        [Required(ErrorMessage = "Quantity is required.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only numbers are allowed.")]
        [Display(Name = "Quantity")]
        public int quantity { get; set; }

        public Nullable<DateTime> CreatedDate { get; set; }

        public Nullable<DateTime> IsEditedDate { get; set; }
    }
}