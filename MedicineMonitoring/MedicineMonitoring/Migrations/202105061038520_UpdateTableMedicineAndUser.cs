namespace MedicineMonitoring.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTableMedicineAndUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Medicines", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.Medicines", "IsEditedDate", c => c.DateTime());
            AlterColumn("dbo.Users", "CreatedDate", c => c.DateTime());
            AlterColumn("dbo.Users", "ModefiedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "ModefiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "CreatedDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Medicines", "IsEditedDate");
            DropColumn("dbo.Medicines", "CreatedDate");
        }
    }
}
