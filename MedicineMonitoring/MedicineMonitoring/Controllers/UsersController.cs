﻿using MedicineMonitoring.Methods;
using MedicineMonitoring.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MedicineMonitoring.Controllers
{
    [AllowAnonymous]
    public class UsersController : Controller
    {
       
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
      //Customer registartion
        public ActionResult UserRegistration()
        {

            User user = new User();
            return View(user);


        }

       
        [HttpPost]

        public ActionResult UserRegistration(User user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {  
                
                using (MedicineDBContext _dbcontext = new MedicineDBContext())
                {
                    User user1 = _dbcontext.Users.Find(user.UserId);
                    if (user1 == null)
                    {
                        user.Approve = true;
                        user.BranchId = null;
                        user.Usercategory = (UserCategory)3;
                        user.CreatedDate = DateTime.UtcNow;
                        
                        _dbcontext.Users.Add(user);
                        methods.Save(_dbcontext);
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        ModelState.AddModelError("", "UserId already exists.");
                        return View();
                    }
                }
            }
        }



    }
}