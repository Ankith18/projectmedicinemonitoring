﻿using MedicineMonitoring.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MedicineMonitoring.Controllers
{
    
    public class MedicineController : Controller
    {
        MedicineMonitoring.Methods.Methods methods = new MedicineMonitoring.Methods.Methods();
        // GET: Medicine
        MedicineDBContext _dbcontext = new MedicineDBContext();
        //List of All Medicine Viewed By Admin
        [Authorize(Roles = "Admin")]
        public ActionResult MedList()
        {
            if (_dbcontext.Medicines.ToList().Count == 0)
            {
                ViewBag.msg = 1;
                return View();
            }
            else
            {
                return View(_dbcontext.Medicines.ToList());
            }
        }
        //Adding Medicine to list By Admin and Branch Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult AddMedicine()
        {
            ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
            ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
            Medicine medicine = new Medicine();
            return View(medicine);
        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        [HttpPost]
        public ActionResult AddMedicine(Medicine medicine)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
                ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
                return View();
            }
            else
            {
               
                HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
                var branch = _dbcontext.Branches.FirstOrDefault(x => x.BranchName == medicine.BranchAvaliable && x.BranchAddress == medicine.Address&&x.BranchId==user.BranchId);

                if (branch != null||user.Usercategory==(UserCategory)1)
                {
                    medicine.CreatedDate = DateTime.UtcNow;
                    _dbcontext.Medicines.Add(medicine);
                    methods.Save(_dbcontext);
                    ViewBag.Message = "Medicine Details Added Succesfully";
                    return View();
                }
                else
                {
                    ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
                    ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
                   
                    ModelState.AddModelError("", "City is Mismatch with Branch");
                    return View();
                }
            }
        
        }
        //Details of Medicine Viewed By both Branch Admin and Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult Details(int? id)
        {
            if (id != null)
            {
                var medicine = _dbcontext.Medicines.Where(x => x.MedId == id).FirstOrDefault();
                return View(medicine);
            }

            return View();

        }
        //Editing details of medicine by both Branch Admin and Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult Edit(int? id)
        {
            Medicine medicine = null;
            if (id != null)
            {
                ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
                ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
                medicine = _dbcontext.Medicines.Where(x => x.MedId == id).FirstOrDefault();

            }

            return View(medicine);

        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        [HttpPost]
        public ActionResult Edit(Medicine medicine)
        {
            if (ModelState.IsValid)
            {
                HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
                var branch = _dbcontext.Branches.FirstOrDefault(x => x.BranchName == medicine.BranchAvaliable && x.BranchAddress == medicine.Address && x.BranchId == user.BranchId);

                if (branch != null|| user.Usercategory == (UserCategory)1)
                {
                    medicine.IsEditedDate = DateTime.UtcNow;
                    _dbcontext.Entry(medicine).State = EntityState.Modified;
                    methods.Save(_dbcontext);
                    ViewBag.Message = "Medicine Details Updated Succesfully";
                    return View();
                }
                else
                {
                   
                         ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
                        ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);

                        ModelState.AddModelError("", "City is Mismatch with Branch");
                        return View();
                   
                }
            }
            ViewBag.BrachList = _dbcontext.Branches.Select(x => x.BranchName);
            ViewBag.BranchAddress = _dbcontext.Branches.Select(x => x.BranchAddress);
            return View();
        }
        //Stock List viwed by Admin
        [Authorize(Roles = "Admin")]
        public ActionResult StockList()
        {
            if (_dbcontext.Medicines.ToList().Count == 0)
            {
                ViewBag.msg = 1;
                return View();
            }
            return View(_dbcontext.Medicines.ToList());
        }
        //Editing stock list by both Branch Admin and Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult EditStock(int? id)
        {
            Medicine medicine = null;
            if (id != null)
            {
                medicine = _dbcontext.Medicines.Where(x => x.MedId == id).FirstOrDefault();

            }

            return View(medicine);

        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        [HttpPost]
        public ActionResult EditStock(Medicine medicine)
        {
            if (ModelState.IsValid)
            {
                _dbcontext.Entry(medicine).State = EntityState.Modified;
                methods.Save(_dbcontext);
                ViewBag.Message = "Medicine Stock Details Updated Succesfully";
                return View();
            }
            return View();
        }

        //Branch stock list Viwed by Admin
        [Authorize(Roles = "Admin")]
        public ActionResult BranchStock()
        {

            return View(_dbcontext.Branches.ToList());
        }
        //Details of each branch Viewed by Admin based on Medicine Type
        [Authorize(Roles = "Admin")]
        public ActionResult ViewEachBranchStock(string id)
        {
            List<Medicine> stockList=new List<Medicine>();
            Branch branch = _dbcontext.Branches.FirstOrDefault(x => x.BranchId == id);
             stockList = _dbcontext.Medicines.Where(x => x.BranchAvaliable == branch.BranchName&&x.Address==branch.BranchAddress).ToList();
            var list = stockList.GroupBy(x => x.Disease).Select(Y => Y.FirstOrDefault()).ToList();
            
            return View(list);
        }
        [Authorize(Roles = "Admin")]
        ////Details of each branch Medicine Viewed by Admin 
        public ActionResult ViewEachBranchMedicine(int id)
        {
            Medicine medicine = _dbcontext.Medicines.FirstOrDefault(x => x.MedId == id);
            var stockList = _dbcontext.Medicines.Where(x => x.Disease==medicine.Disease&&x.BranchAvaliable==medicine.BranchAvaliable&&x.Address==medicine.Address).ToList();

            return View(stockList);
        }
        //Reports Viewed by Admin
        [Authorize(Roles = "Admin")]
        public ActionResult GenenerateReport()
        {
            ViewBag.BranchList = _dbcontext.Branches.Select(x => x.BranchId);
            Branch branch = new Branch();
            return View(branch);
        }
        //Reports Viewed by Admin based on brand,stock,illness and expiry Date
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult SalesReport(Branch branch)
        {
            ViewBag.BranchId = branch.BranchId;
            return View();
        }
        //Report of Stock Viewed by both Admin and Branch Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult StocksReport(string id)
        {
            Branch branchObj = _dbcontext.Branches.FirstOrDefault(t => t.BranchId == id);
            var list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branchObj.BranchName && t.Address == branchObj.BranchAddress);
            return View(list);

        }
        //Report of Brand Viewed by both Admin and Branch Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult BrandsReport(string id)
        {
            Branch branchObj = _dbcontext.Branches.FirstOrDefault(t => t.BranchId == id);
            var list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branchObj.BranchName && t.Address == branchObj.BranchAddress);
            return View(list); 
        }
        //Report of Sales Viewed by both Admin and Branch Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult SaleReport(string id)
        {
            List<OrderItem> orderItems = new List<OrderItem>();
            Branch branchObj = _dbcontext.Branches.FirstOrDefault(t => t.BranchId == id);
            List<Medicine> list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branchObj.BranchName && t.Address == branchObj.BranchAddress).ToList();
            foreach(var x in list)
            {
                OrderItem orderItem = _dbcontext.OrderItems.FirstOrDefault(y => y.MedId == x.MedId);
                orderItems.Add(orderItem);
            }
          
            return View(orderItems);
        }
        //Report of Illness Viewed by both Admin and Branch Admin
        [Authorize(Roles = "Admin,BranchAdmin")]
        public ActionResult IllnesseReport(string id)
        {
            Branch branchObj = _dbcontext.Branches.FirstOrDefault(t => t.BranchId == id);
            var list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branchObj.BranchName && t.Address == branchObj.BranchAddress);
            return View(list);
        }
        [Authorize(Roles = "Admin,BranchAdmin")]
        //Report of Expiry Date Viewed by both Admin and Branch Admin
        public ActionResult ExperiDateReport(string id)
        {
            Branch branchObj = _dbcontext.Branches.FirstOrDefault(t => t.BranchId == id);
            var list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branchObj.BranchName && t.Address == branchObj.BranchAddress);
            return View(list);
        }
        //Each branch Medicines viewd by Branch Admin
        [Authorize(Roles = "BranchAdmin")]

        public ActionResult BranchMedList()
        {
           
            HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
            Branch branch = _dbcontext.Branches.FirstOrDefault(y => y.BranchId == user.BranchId);

            var list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branch.BranchName && t.Address == branch.BranchAddress);
            if (list==null)
            {
                ViewBag.msg = 1;
                return View();
            }
            return View(list);
        }
        //Each branch Medicines stocks viewd by Branch Admin
        [Authorize(Roles = "BranchAdmin")]
        public ActionResult BranchStockList()
        {

            HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
            Branch branch = _dbcontext.Branches.FirstOrDefault(y => y.BranchId == user.BranchId);

            var list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branch.BranchName && t.Address == branch.BranchAddress);
            if (list == null)
            {
                ViewBag.msg = 1;
                return View();
            }
            return View(list);
        }
        //Reports Viewed by Branch Admin based on brand,stock,illness and expiry Date
        [Authorize(Roles = "BranchAdmin")]
       
        public ActionResult BranchAdminReport()
        {
            HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
            Branch branch = _dbcontext.Branches.FirstOrDefault(y => y.BranchId == user.BranchId);
            ViewBag.BranchId = branch.BranchId;

            return View();
        }

        //Staus of Stocks based on quantity Viewed By Branch Admin
        [Authorize(Roles = "BranchAdmin")]
        public ActionResult MedicineStatus()
        {
            HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            User user = _dbcontext.Users.FirstOrDefault(y => y.UserId == authTicket.Name);
            Branch branch = _dbcontext.Branches.FirstOrDefault(y => y.BranchId == user.BranchId);

            var list = _dbcontext.Medicines.Where(t => t.BranchAvaliable == branch.BranchName && t.Address == branch.BranchAddress);
           
            return View(list);
        }
    }
}